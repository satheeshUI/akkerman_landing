(function() {
    // timer
    var countDownDate;

    if(document.getElementById("singlePageTime")) {
        countDownDate = new Date(document.getElementById("singlePageTime").dataset.timestamp).getTime()
    }

    if($("#countDownTime").length) {
        countDownDate = new Date($("#countDownTime").attr('data-timestamp')).getTime()
    }

    if(countDownDate) {
        // Update the count down every 1 second
        var x = setInterval(function() {
    
        // Get today's date and time
        var now = new Date().getTime();
            
        // Find the distance between now and the count down date
        var distance = countDownDate - now;
            
        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    
        let timer = document.getElementById("timer")

        if(document.getElementById("singlePageTime")) {
            document.getElementById("singlePageTime").innerHTML = days + ' : ' + hours + ' : ' + minutes
        }
    
        if(timer) {
            let daysElm = timer.querySelector("#days")
            let hoursElm = timer.querySelector("#hours")
            let minutesElm = timer.querySelector("#minutes")
            daysElm.children[0].innerHTML = days
            hoursElm.children[0].innerHTML = hours
            minutesElm.children[0].innerHTML = minutes
        }
            
        // If the count down is over, write some text 
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("timer").innerHTML = "EXPIRED";
        }
        }, 1000);
    }

    $('.singlepagecarousel').owlCarousel({
        items: 1,
        loop: true,
        dots: true
      })
    $('.logoslide__carousel').owlCarousel({
        items: 3,
        loop: true,
        dots: false, 
        nav: true,
        margin: 60
    })
})();

function scrolldown() {
    let section = document.getElementsByClassName("bannersection")[0]
    let header = document.getElementsByTagName("header")
    let height;

    if(this.outerWidth < 992) {
        height = section.clientHeight + header[0].clientHeight
    } else {
        height = section.clientHeight-120
    }
    $("html, body").animate({scrollTop: height}, 1000)

}